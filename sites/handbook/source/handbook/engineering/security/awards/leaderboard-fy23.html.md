---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kassio](https://gitlab.com/kassio) | 1 | 600 |
| [@brodock](https://gitlab.com/brodock) | 2 | 600 |
| [@leipert](https://gitlab.com/leipert) | 3 | 500 |
| [@.luke](https://gitlab.com/.luke) | 4 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 5 | 500 |
| [@garyh](https://gitlab.com/garyh) | 6 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 7 | 400 |
| [@vitallium](https://gitlab.com/vitallium) | 8 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 9 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 10 | 300 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 11 | 300 |
| [@ratchade](https://gitlab.com/ratchade) | 12 | 300 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 13 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 14 | 120 |
| [@toupeira](https://gitlab.com/toupeira) | 15 | 120 |
| [@10io](https://gitlab.com/10io) | 16 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 17 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 18 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 19 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 20 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 21 | 60 |
| [@minac](https://gitlab.com/minac) | 22 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 23 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 24 | 60 |
| [@pshutsin](https://gitlab.com/pshutsin) | 25 | 40 |
| [@ghickey](https://gitlab.com/ghickey) | 26 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 27 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 28 | 30 |
| [@subashis](https://gitlab.com/subashis) | 29 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 30 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 31 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 32 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 33 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kassio](https://gitlab.com/kassio) | 1 | 600 |
| [@brodock](https://gitlab.com/brodock) | 2 | 600 |
| [@leipert](https://gitlab.com/leipert) | 3 | 500 |
| [@.luke](https://gitlab.com/.luke) | 4 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 5 | 500 |
| [@garyh](https://gitlab.com/garyh) | 6 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 7 | 400 |
| [@vitallium](https://gitlab.com/vitallium) | 8 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 9 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 10 | 300 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 11 | 300 |
| [@ratchade](https://gitlab.com/ratchade) | 12 | 300 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 13 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 14 | 120 |
| [@toupeira](https://gitlab.com/toupeira) | 15 | 120 |
| [@10io](https://gitlab.com/10io) | 16 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 17 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 18 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 19 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 20 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 21 | 60 |
| [@minac](https://gitlab.com/minac) | 22 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 23 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 24 | 60 |
| [@pshutsin](https://gitlab.com/pshutsin) | 25 | 40 |
| [@ghickey](https://gitlab.com/ghickey) | 26 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 27 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 28 | 30 |
| [@subashis](https://gitlab.com/subashis) | 29 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 30 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 31 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 32 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 33 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |


