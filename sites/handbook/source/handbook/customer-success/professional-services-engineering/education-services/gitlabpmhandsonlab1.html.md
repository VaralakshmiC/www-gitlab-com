---
layout: handbook-page-toc
title: "GitLab Project Management Hands On Guide- Lab 1"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab Project Management course."
---
# GitLab Project Management Hands On Guide- Lab 1
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## LAB 1- REVIEW EXAMPLE GITLAB ISSUE SECTION
1. Navigate to the following URL- <https://gitlab.com/gitlab-org/gitlab/-/issues>  
2. Review the Issues List, Boards, Milestones and Labels.
3. Hover over any of the columns and rows in the following sections to view more details.  
4. Click on the **Boards** > **Switchboard** dropdown > 12.3 Release
5. Review all of the Boards that display and the available filtering options.  
6. Review any **of the issues** and take a look at the labels used throughout the project.  

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab Project Management- please submit your changes via Merge Request!

